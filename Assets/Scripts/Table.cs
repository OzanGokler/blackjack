﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;

public class Table : MonoBehaviour {
    
    public Text txt_balance;

    void Awake ()
    {
        GameStatic.table = this;        
    }

    void Start()
    {
        UpdateChipText(GameStatic.metaGame.PlayerMoney);
    }

    public void UpdateChipText(double value)
    {
        txt_balance.text = value.ToString();
    }

}
