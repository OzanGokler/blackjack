﻿using UnityEngine;
using System.Collections;

public class Control : MonoBehaviour {

    [SerializeField] private MouseLook m_MouseLook;
    [SerializeField]
    private bool ShowCursor = false;
    [SerializeField]
    private CursorLockMode lockState;

    public Transform parent;
    
    // Use this for initialization
    void Start () {
        //Debug.Log("transform: " + transform.position);
        Cursor.visible = ShowCursor;
        Cursor.lockState = lockState;
        m_MouseLook.Init(parent, transform);
    }
	
	// Update is called once per frame
	void Update ()
    {
        m_MouseLook.LookRotation(parent, transform);
    }
}
