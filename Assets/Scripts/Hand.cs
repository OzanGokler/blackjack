﻿using System.Linq;
using System.Collections.Generic;
using System;

public class Hand
{
    public List<Card> Cards;

    public Card LastCard { get { return Cards.Last(); } }

    public CardPosition CardType { get; private set; }

    public bool IsDone;
    public double Chips;
    public HandState Result;

    public Hand(CardPosition cardType)
    {
        CardType = cardType;
        Cards = new List<Card>();
    }
    

    public void ResetHand()
    {
        Cards.Clear();
        IsDone = false;
    }

    public bool IsBlackjack { get { return Cards.Count == 2 && Cards.Select(c => c.Value).Sum() == 21; } }

    public bool CanSplit
    {
        get { return CardType == CardPosition.Player && Cards.Count == 2 && Cards[0].Value == Cards[1].Value; }
    }

    public int SoftValue
    {
        get { return this.Cards.Select(c => (int)c.Value > 1 && (int)c.Value < 12 ? (int)c.Value : 10).Sum(); }
    }

    public int TotalValue
    {
        get
        {
            var totalValue = this.SoftValue;
            var aces = this.Cards.Count(c => c.Value == 11);

            while (totalValue > 21 && aces-- > 0)
            {
                totalValue -= 10;
            }

            return totalValue;
        }
    }

    public bool HasInsurance { get { return Cards[0].Value == 11; } }
}

public enum HandState
{
    None,
    Win,
    Lose,
    Blackjack,
    Push
}