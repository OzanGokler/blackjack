﻿using UnityEngine;
using System.Collections;
using Newtonsoft.Json;
using System.Collections.Generic;
using System;

public class Deck : MonoBehaviour {

    public List<Card> cardDeck;
    
    public void Start()
    {
        GameStatic.metaGame.Reset += MetaGame_Reset;
    }

    private void MetaGame_Reset()
    {
        LoadNewSet();
    }

    public void LoadNewSet()
    {
        cardDeck = new List<Card>();
        for (int i = 0; i < 4; i++)
        {           
            for (int j = 1; j <= 13; j++)
            {
                Card _card = new Card();
                _card.suit = (Card.Suit)i;
                _card.number = j;                
                cardDeck.Add(_card);
            }
        }
    }
    
    public Card GetSafeCard()
    {
        int currentIndex = UnityEngine.Random.Range(0, cardDeck.Count);

        //TODO: You might want to look here for errors. If currentIndex is less than zero or different cases.
        var card = cardDeck[currentIndex];

        cardDeck.RemoveAt(currentIndex);
        return card;
    }
}
