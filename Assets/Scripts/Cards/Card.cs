﻿using UnityEngine;
using System.Collections;
using System;

public class Card
{
    public enum Suit
    {
        Spades,
        Diamonds,
        Hearts,
        Clubs,
    }

    public Suit suit = Suit.Diamonds;
    public int number = 1;

    public int Value
    {
        get
        {
            switch (number)
            {
                case 1:
                    return 11;
                case 11:
                case 12:
                case 13:
                    return 10;
                default:
                    return number;
            }
        }
    }

    internal Vector2 GettextureOffset()
    {
        var suitOffset = (int)suit;
        var cardIndex = suitOffset * 13 + number;

        int cardX = (cardIndex - 1) % 10;
        int cardY = (cardIndex - 1) / 10;

        return new Vector2(cardX * 0.1f, cardY * -0.14f);
    }
}
