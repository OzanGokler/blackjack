﻿using UnityEngine;
using System.Collections;
using System;

public class CardPlacement : MonoBehaviour {

    public CardObjectManager CardObjects;
    public GameObject[] DealerCardPos;
    public GameObject[] PlayerCardPos;
    public GameObject[] SplitCardPos;
    public float CardScale;

    private CardObject[] DealerCards = new CardObject[9];
    private CardObject[] PlayerCards = new CardObject[9];
    private CardObject[] SplitCards = new CardObject[9];

    private Transform[] DealerInitialPositions = new Transform[2];
     
    // Use this for initialization
    void Awake () {
        GameStatic.CardPlacementManager = this;
        DealerInitialPositions[0] = DealerCardPos[0].transform;
        DealerInitialPositions[1] = DealerCardPos[1].transform;
    }

    void Start()
    {
        GameStatic.metaGame.Reset += MetaGame_Reset;
    }

    private void MetaGame_Reset()
    {
        DisableAllCards();
        DealerCardPos[0].transform.position = DealerInitialPositions[0].position;
        DealerCardPos[0].transform.rotation = DealerInitialPositions[0].rotation;
        DealerCardPos[1].transform.position = DealerInitialPositions[1].position;
        DealerCardPos[1].transform.rotation = DealerInitialPositions[1].rotation;
    }

    public void DisableAllCards()
    {
        ClearDealerCards();
        ClearPlayerCards();
        ClearSplitCards();
        DealerCardPos[1].transform.Rotate(new Vector3(180, 0, 0));
    }

    public void AddCard(Card c, CardPosition cardPosition)
    {
        int iCount = -1;
        GameObject Bone = null;

        if (cardPosition == CardPosition.Dealer)
        {
            iCount = GameStatic.animationManager.GetDealerCardCount() - 1;
            Bone = DealerCardPos[iCount];
        }
        else if (cardPosition == CardPosition.Player)
        {
            iCount = GameStatic.animationManager.GetPlayerCardCount() - 1;
            Bone = PlayerCardPos[iCount];
        }
        else if(cardPosition == CardPosition.PlayerSplit)
        {
            iCount = GameStatic.animationManager.GetSplitCardCount();
            Bone = SplitCardPos[iCount];
        }        
        
        GameObject go = CardObjects.GetCard(c);

        //TODO: use pooling. We don't need to instantiate every card.
        //TODO: actually we need refactor here. Too many getcomponent function
        GameObject goNewCard = Instantiate(go) as GameObject;
        var NewCard = goNewCard.GetComponent<CardObject>();

        NewCard.transform.localScale = new Vector3 (CardScale, CardScale, CardScale);
        NewCard.transform.SetParent(Bone.transform);
        NewCard.transform.localPosition = new Vector3();
        NewCard.transform.localRotation = new Quaternion();

        foreach (Transform child in NewCard.transform)
        {
            if (child.name == "Front")
            {
                NewCard.Front = child.gameObject;
            }
            else if (child.name == "Back")
            {
                NewCard.Back = child.gameObject;
            }
        }

        if (cardPosition == CardPosition.Dealer && GameStatic.metaGame.IsSecondDealerCard)
        {
            NewCard.SetLocation(iCount + 1);
        }
        else
            NewCard.SetLocation(iCount);

        switch (cardPosition)
        {
            case CardPosition.Dealer:
                DealerCards[iCount] = NewCard;
                break;
            case CardPosition.Player:
                PlayerCards[iCount] = NewCard;
                break;
            case CardPosition.PlayerSplit:
                SplitCards[iCount] = NewCard;
                break;
            default:
                break;
        }
    }
    
    internal void ClearDealerCards()
    {
        //disable dealer cards
        for (int i = 0; i < DealerCards.Length; i++)
        {
            if (DealerCards[i] != null)
            {
                Destroy(DealerCards[i].gameObject);
                DealerCards[i] = null;
            }
        }
    }

    internal void ClearPlayerCards()
    {
        //disable player cards
        for (int i = 0; i < PlayerCards.Length; i++)
        {
            if (PlayerCards[i] != null)
            {
                Destroy(PlayerCards[i].gameObject);
                PlayerCards[i] = null;
            }
        }
    }

    internal void ClearSplitCards()
    {
        //disable split cards
        for (int i = 0; i < SplitCards.Length; i++)
        {
            if (SplitCards[i] != null)
            {
                Destroy(SplitCards[i].gameObject);
                SplitCards[i] = null;
            }
        }
    }

    internal void SplitCard()
    {
        var card = PlayerCards[1];
        SplitCards[0] = card;
        SplitCards[0].transform.SetParent(SplitCardPos[0].transform);
        SplitCards[0].transform.localPosition = new Vector3();
        SplitCards[0].transform.localRotation = new Quaternion();
        SplitCards[0].SetLocation(-3);
        GameStatic.animationManager.AddSplitCardCount();
        PlayerCards[1] = null;
    }
}

public enum CardPosition
{
    Dealer,
    Player,
    PlayerSplit
}
