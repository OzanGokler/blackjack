﻿using UnityEngine;
using System.Collections;

public class Chip
{
    public int Five = 0;
    public int TwentyFive = 0;
    public int Hundred = 0;
    public int FiveHundred = 0;
    public int Thousand = 0;
    public int FiveThousand = 0;

    public int GetTotal()
    {
        return Five * 5 + TwentyFive * 25 + Hundred * 100 + FiveHundred * 500 + Thousand * 1000 + FiveThousand * 5000;
    }
}
