﻿using UnityEngine;
using System.Collections;
using System;

public enum SwipeDirection
{
    None = 0,
    Up = 4,
    Down = 8
}

public class SwipeManager : MonoBehaviour {

    public event Action OnClick;
    public event Action<SwipeDirection> OnSwipe;

    public SwipeDirection Direction { set; get; }

    private Vector3 touchPosition;
    private float swipeResistanceY = 100f;

	void Awake ()
    {
        GameStatic.swipeManager = this;
	}
	
	// Update is called once per frame
	void Update () {

        Direction = SwipeDirection.None;

        if(Input.GetMouseButtonDown(0))
        {
            touchPosition = Input.mousePosition;
        }
        if (Input.GetMouseButtonUp(0))
        {
            Vector2 deltaSwipe = touchPosition - Input.mousePosition;
            Direction = SwipeDirection.None;
            if (Mathf.Abs(deltaSwipe.y) > swipeResistanceY)
            {
                // Swipe on the Y axis
                Direction |= (deltaSwipe.y < 0) ? SwipeDirection.Up : SwipeDirection.Down;
            }

            if (Direction == SwipeDirection.Up)
            {
                if (OnSwipe != null)
                {
                    OnSwipe(SwipeDirection.Up);
                }
            }
            else if(Direction == SwipeDirection.Down)
            {
                if (OnSwipe != null)
                {
                    OnSwipe(SwipeDirection.Down);
                }
            }
            else
            {
                if (OnClick != null)
                {
                    OnClick();
                }
            }
        }



    }
}
