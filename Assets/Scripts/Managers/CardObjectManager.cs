﻿using UnityEngine;
using System.Collections;

public class CardObjectManager : MonoBehaviour {

    public CardSuite CardsHearts;
    public CardSuite CardsSpades;
    public CardSuite CardsDiamonds;
    public CardSuite CardsClubs;
    
    public GameObject GetCard(Card c)
    {
        GameObject go = CardsHearts.Cards[0];
        int CardIndex = c.number - 1;

        switch (c.suit)
        {
            case Card.Suit.Diamonds:
                go = CardsDiamonds.Cards[CardIndex];
                break;
            case Card.Suit.Hearts:
                go = CardsHearts.Cards[CardIndex];
                break;
            case Card.Suit.Clubs:
                go = CardsClubs.Cards[CardIndex];
                break;
            case Card.Suit.Spades:
                go = CardsSpades.Cards[CardIndex];
                break;
            default:
                break;
        }

        return go;
    }


}
