﻿using UnityEngine;
using System.Collections;
using System;

public class AnimationManager : MonoBehaviour
{
    public Animator Dealer;
    public Animator Face;
    public Animator Cards;
    public Animator UI;
    public AnimationStringManager AnimStringMgr;

    void Awake()
    {
        GameStatic.animationManager = this;
        AnimStringMgr = new AnimationStringManager();
    }

    void Start()
    {
        GameStatic.metaGame.Reset += MetaGame_Reset;
    }

    private void MetaGame_Reset()
    {
        ResetCardCounts();
    }

    //////// DEALER ////////
    #region DEALER ANIMATIONS
    public int GetPlayerCardCount()
    {
        return Dealer.GetInteger("PlayerCardCount");
    }

    public void AddCardCount(CardPosition pos)
    {
        string varName = null;

        switch (pos)
        {
            case CardPosition.Dealer: varName = "DealerCardCount"; break;
            case CardPosition.Player: varName = "PlayerCardCount"; break;
            case CardPosition.PlayerSplit: varName = "SplitCardCount"; break;
            default:
                break;
        }

        Dealer.SetInteger(varName, Dealer.GetInteger(varName) + 1);
    }

    public void AddPlayerCardCount()
    {
        Dealer.SetInteger("PlayerCardCount", Dealer.GetInteger("PlayerCardCount") + 1);
    }

    public int GetDealerCardCount()
    {
        return Dealer.GetInteger("DealerCardCount");
    }
    public void AddDealerCardCount()
    {
        Dealer.SetInteger("DealerCardCount", Dealer.GetInteger("DealerCardCount") + 1);
    }

    public int GetSplitCardCount()
    {
        return Dealer.GetInteger("SplitCardCount");
    }
    public void AddSplitCardCount()
    {
        Dealer.SetInteger("SplitCardCount", Dealer.GetInteger("SplitCardCount") + 1);
    }

    public void ResetCardCounts()
    {
        Dealer.SetInteger("DealerCardCount", 0);
        Dealer.SetInteger("PlayerCardCount", 0);
    }

    public void HandStart()
    {
        Dealer.SetTrigger("Deal");
    }

    public void HandFinish()
    {
        Dealer.SetTrigger("Hand-Finished");
    }

    public void GetPlayerCard()
    {
        Dealer.SetTrigger("Deal");
    }
    public void StartDealerTurn()
    {
        Dealer.SetTrigger("Stand");
    }
    public void GetDealerCard()
    {
        Dealer.SetTrigger("Deal");
    }

    internal void GetSplitCard()
    {
        Dealer.SetTrigger("SplitDeal");
    }
    internal void SetDealerInsurance(bool v)
    {
        Dealer.SetBool("Insurance", v);
    }
    #endregion

    //////// CARDS ////////
    #region CARD ANIMATIONS
    public void PlayFlipCard()
    {
        Cards.Play("Flip-DealerCard2");
    }


    public void PlayDealDealerCard2()
    {
        Cards.Play("Deal-DealerCard2");
    }
    #endregion

    //////// UI //////////
    #region UI ANIMATIONS
    //TODO: Refactor here. Use animation trigger system for UI animations. 
    public void UI_bet5()
    {
        UI.Play("UI_bet5");
    }

    public void UI_bet5_onPress()
    {
        UI.Play("UI_bet5_onpress");
    }
    public void UI_bet25()
    {
        UI.Play("UI_bet25");
    }
    public void UI_bet25_onPress()
    {
        UI.Play("UI_bet25_onpress");
    }
    public void UI_bet100()
    {
        UI.Play("UI_bet100");
    }
    public void UI_bet100_onPress()
    {
        UI.Play("UI_bet100_onpress");
    }
    public void UI_bet500()
    {
        UI.Play("UI_bet500");
    }
    public void UI_bet500_onPress()
    {
        UI.Play("UI_bet500_onpress");
    }
    public void UI_bet1000()
    {
        UI.Play("UI_bet1000");
    }
    public void UI_bet1000_onPress()
    {
        UI.Play("UI_bet1000_onpress");
    }
    public void UI_bet5000()
    {
        UI.Play("UI_bet5000");
    }
    public void UI_bet5000_onPress()
    {
        UI.Play("UI_bet5000_onpress");
    }
    public void UI_Clear()
    {
        UI.Play("UI_clear");
    }
    public void UI_Clear_onPress()
    {
        UI.Play("UI_clear_onpress");
    }
    public void UI_Deal()
    {
        UI.Play("UI_deal");
    }
    public void UI_Deal_onPress()
    {
        UI.Play("UI_deal_onpress");
    }
    public void UI_Stand()
    {
        UI.Play("UI_stand");
    }
    public void UI_Stand_onPress()
    {
        UI.Play("UI_stand_onpress");
    }
    public void UI_Hit()
    {
        UI.Play("UI_hit");
    }
    public void UI_Hit_onPress()
    {
        UI.Play("UI_hit_onpress");
    }

    internal void UI_Split()
    {
        UI.Play("UI_split");
    }

    internal void UI_Split_onPress()
    {
        UI.Play("UI_split_onPress");
    }

    internal void UI_Yes()
    {
        UI.Play("UI_yes");
    }

    internal void UI_No()
    {
        UI.Play("UI_no");
    }

    internal void AfterSplit()
    {
        Dealer.SetBool("Split", false);
        Dealer.SetTrigger("Stand");
    }
    #endregion

    //////// DEALER FACIAL EXPRESSIONS ////////
    #region Dealer Facial Expressions

    public void PlayDefaultExpression()
    {
        Dealer.Play(AnimStringMgr.DEALER_FACIAL_EXP_DEFAULT);
    }

    internal void SetDealerBlackjackResult(bool v)
    {
        Dealer.SetBool("InsuranceResult", v);
    }


    #endregion
}