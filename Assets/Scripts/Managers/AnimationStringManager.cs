﻿using UnityEngine;
using System.Collections;

public class AnimationStringManager
{
    public int DEALER_FACIAL_EXP_DEFAULT;
    public int DEALER_FACIAL_EXP_FLIRTY;
    public int DEALER_FACIAL_EXP_VERYSORRY;
    public int DEALER_FACIAL_EXP_CRINGE;
    public int DEALER_FACIAL_EXP_CRINGE1;
    public int DEALER_FACIAL_EXP_CONFUSED;
    public int DEALER_FACIAL_EXP_SORRY;
    public int DEALER_FACIAL_EXP_IMPRESSIVE;
    public int DEALER_FACIAL_EXP_UNIMPRESSED;
    public int DEALER_FACIAL_EXP_SUSPICIOUS;
    public int DEALER_FACIAL_EXP_DISSAPOINTMENT;
    public int DEALER_FACIAL_EXP_HAPPYSURPRISED;
    public int DEALER_FACIAL_EXP_UNSURE;
    public int DEALER_FACIAL_EXP_WORRIED;
    public int DEALER_FACIAL_EXP_SUSPICIOUS1;

    public AnimationStringManager()
    {
        DEALER_FACIAL_EXP_DEFAULT = Animator.StringToHash("Exp_default_01");
        DEALER_FACIAL_EXP_FLIRTY = Animator.StringToHash("Exp_flirty_01");
        DEALER_FACIAL_EXP_VERYSORRY = Animator.StringToHash("Exp_verysorry_01");
        DEALER_FACIAL_EXP_CRINGE = Animator.StringToHash("Exp_cringe_01");
        DEALER_FACIAL_EXP_CRINGE1 = Animator.StringToHash("Exp_cringe_02");
        DEALER_FACIAL_EXP_CONFUSED = Animator.StringToHash("Exp_confused_01");
        DEALER_FACIAL_EXP_SORRY = Animator.StringToHash("Exp_sorry_01");
        DEALER_FACIAL_EXP_IMPRESSIVE = Animator.StringToHash("Exp_impressive_01");
        DEALER_FACIAL_EXP_UNIMPRESSED = Animator.StringToHash("Exp_unimpressed_01");
        DEALER_FACIAL_EXP_SUSPICIOUS = Animator.StringToHash("Exp_suspicious_01");
        DEALER_FACIAL_EXP_DISSAPOINTMENT = Animator.StringToHash("Exp_dissapointment_01");
        DEALER_FACIAL_EXP_HAPPYSURPRISED = Animator.StringToHash("Exp_happysurprised_01");
        DEALER_FACIAL_EXP_UNSURE = Animator.StringToHash("Exp_unsure_01");
        DEALER_FACIAL_EXP_WORRIED = Animator.StringToHash("Exp_worried_01");
        DEALER_FACIAL_EXP_SUSPICIOUS1 = Animator.StringToHash("Exp_suspicious_02");
    }
}
