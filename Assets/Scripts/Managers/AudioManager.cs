﻿using UnityEngine;
using RogoDigital.Lipsync;
using System;

public class AudioManager : MonoBehaviour {

    public bool CasinoLoopPlayOnAwake;
    public LipSync _DealerVOSource;

    public LipSyncData lsWelcome;
    public LipSyncData lsTwo;
    public LipSyncData lsThree;
    public LipSyncData lsFour;
    public LipSyncData lsFive;
    public LipSyncData lsSix;
    public LipSyncData lsSeven;
    public LipSyncData lsEight;
    public LipSyncData lsNine;
    public LipSyncData lsTen;
    public LipSyncData lsEleven;
    public LipSyncData lsTwelve;
    public LipSyncData lsThirteen;
    public LipSyncData lsFourteen;
    public LipSyncData lsFifteen;
    public LipSyncData lsSixteen;
    public LipSyncData lsSeventeen;
    public LipSyncData lsEighteen;
    public LipSyncData lsNineteen;
    public LipSyncData lsTwenty;
    public LipSyncData lsTwentyone;
    public LipSyncData lsSoftThirteen;
    public LipSyncData lsSoftFourteen;
    public LipSyncData lsSoftFifteen;
    public LipSyncData lsSoftSixteen;
    public LipSyncData lsSoftSeventeen;
    public LipSyncData lsSoftEighteen;
    public LipSyncData lsSoftNineteen;
    public LipSyncData lsSoftTwenty;
    public LipSyncData lsDoubleAces;


    public LipSyncData[] lsOfferInsurance;

    public LipSyncData lsAhh;
    public LipSyncData lsAww;
    public LipSyncData lsHmm;
    public LipSyncData lsSoClose;

    public LipSyncData[] lsDealerBlackjack;

    public LipSyncData[] lsDealerBust;

    public LipSyncData lsDealerWins;
    public LipSyncData lsDealerTwentyOne;

    public LipSyncData[] lsHighBet;
    public LipSyncData[] lsNewHand;
    public LipSyncData[] lsPlayerBlackjack;
    public LipSyncData[] lsPlayerBust;
    public LipSyncData[] lsPlayerWins;
    public LipSyncData[] lsPlayerWinStreak;
    public LipSyncData[] lsPush;


    public AudioSource _CardSource;
    public AudioClip CardFlip;
    public AudioClip CardPlaceCard;
    public AudioClip CardPlacetable;
    public AudioClip CardCardShoe;
    public AudioClip CardGatherCards;

    public AudioSource _CasinoSource;

    public AudioSource _UISource;
    public AudioClip UINav;
    public AudioClip UISelect;

    public AudioSource ChipSource;
    public AudioClip fxChips;

    public AudioSource GamingSource;
    public AudioClip fxWin;    
    

    void Awake()
    {
        GameStatic.AudioManager = this;
    }

	// Use this for initialization
	void Start () {
        PlayHello();
        if (CasinoLoopPlayOnAwake)
        {
            _CasinoSource.Play();
        }
	}

    #region CARDS
    public void PlayCardFlip()
    {
        _CardSource.PlayOneShot(CardFlip);
    }

    public void PlayPlaceCard()
    {
        _CardSource.PlayOneShot(CardPlaceCard);
    }

    public void PlayPlaceCardTable()
    {
        _CardSource.PlayOneShot(CardPlacetable);
    }

    public void PlayCardShoe()
    {
        _CardSource.PlayOneShot(CardCardShoe);
    }

    public void PlayGatherCards()
    {
        _CardSource.PlayOneShot(CardGatherCards);
    }
#endregion

    #region UI
    public void PlayUISelect()
    {
        _UISource.PlayOneShot(UISelect);
    }

    public void PlayUINavigation()
    {
        _UISource.PlayOneShot(UINav);
    }

    #endregion

    #region VO

    public void PlayHello()
    {
        _DealerVOSource.Play(lsWelcome);
    }

    internal void PlayFinishHand(HandState result, Hand playerHand, Hand dealerHand)
    {
        switch (result)
        {
            case HandState.Win:
                GamingSource.PlayOneShot(fxWin);

                if (playerHand.IsBlackjack)
                {
                    PlayPlayerBlackjack();
                }
                else if (dealerHand.SoftValue > 21)
                {
                    PlayDealerBusts();
                }
                else
                {
                    PlayPlayerWins();
                }
                break;
            case HandState.Lose:
                if (dealerHand.IsBlackjack)
                {
                    PlayDealerBlackjack();
                }
                else if (dealerHand.TotalValue == 21)
                {
                    _DealerVOSource.Play(lsDealerTwentyOne);
                }
                else if (playerHand.TotalValue > 21)
                {
                    PlayPlayerBusts();
                }
                else
                {
                    if (dealerHand.SoftValue - playerHand.SoftValue == 1)
                    {
                        _DealerVOSource.Play(lsSoClose);
                        break;
                    }
                    PlayDealerWins();
                }
                break;
            case HandState.Blackjack:
                PlayPlayerBlackjack();
                break;
            case HandState.Push:
                PlayPush();
                break;
            default:
                break;
        }
    }

    internal void PlayInitialHandValue(Hand playerHand)
    {
        var card0 = playerHand.Cards[0];
        var card1 = playerHand.Cards[1];
                
        if (card0.number == 1 || card1.number == 1) ///We have soft hand
        {
            switch (playerHand.TotalValue)
            {
                case 13: _DealerVOSource.Play(lsSoftThirteen); break;
                case 14: _DealerVOSource.Play(lsSoftFourteen); break;
                case 15: _DealerVOSource.Play(lsSoftFifteen); break;
                case 16: _DealerVOSource.Play(lsSoftSixteen); break;
                case 17: _DealerVOSource.Play(lsSoftSeventeen); break;
                case 18: _DealerVOSource.Play(lsSoftEighteen); break;
                case 19: _DealerVOSource.Play(lsSoftNineteen); break;
                case 20: _DealerVOSource.Play(lsSoftTwenty);  break;
                default:
                    break;
            }
        }
        else
        {
            switch (playerHand.TotalValue)
            {
                case 2: _DealerVOSource.Play(lsDoubleAces); break;
                case 3: _DealerVOSource.Play(lsThree); break;
                case 4: _DealerVOSource.Play(lsFour); break;
                case 5: _DealerVOSource.Play(lsFive); break;
                case 6: _DealerVOSource.Play(lsSix); break;
                case 7: _DealerVOSource.Play(lsSeven); break;
                case 8: _DealerVOSource.Play(lsEight); break;
                case 9: _DealerVOSource.Play(lsNine); break;
                case 10: _DealerVOSource.Play(lsTen); break;
                case 11: _DealerVOSource.Play(lsEleven); break;
                case 12: _DealerVOSource.Play(lsTwelve); break;
                case 13: _DealerVOSource.Play(lsThirteen); break;
                case 14: _DealerVOSource.Play(lsFourteen); break;
                case 15: _DealerVOSource.Play(lsFifteen); break;
                case 16: _DealerVOSource.Play(lsSixteen); break;
                case 17: _DealerVOSource.Play(lsSeventeen); break;
                case 18: _DealerVOSource.Play(lsEighteen); break;
                case 19: _DealerVOSource.Play(lsNineteen); break;
                case 20: _DealerVOSource.Play(lsTwenty); break;
                case 21: _DealerVOSource.Play(lsTwentyone); break;
                default:
                    break;
            }
        }
    }
    

    private void PlayRandomAudioFromArray(LipSyncData[] lsArr)
    {
        var rnd = UnityEngine.Random.Range(0, lsArr.Length);
        _DealerVOSource.Play(lsArr[rnd]);
    }

    public void PlayOfferInsurance()
    {
        PlayRandomAudioFromArray(lsOfferInsurance);
    }

    public void PlayPlayerBusts()
    {
        PlayRandomAudioFromArray(lsPlayerBust);
    }

    public void PlayPlayerWins()
    {
        PlayRandomAudioFromArray(lsPlayerWins);
    }

    public void PlayPlayerWinStreak()
    {
        PlayRandomAudioFromArray(lsPlayerWinStreak);
    }

    public void PlayPlayerBlackjack()
    {
        PlayRandomAudioFromArray(lsPlayerBlackjack);
    }

    public void PlayDealerWins()
    {
        _DealerVOSource.Play(lsDealerWins);
    }

    public void PlayDealerBusts()
    {
        PlayRandomAudioFromArray(lsDealerBust);
    }

    public void PlayDealerBlackjack()
    {
        _DealerVOSource.Play(lsDealerTwentyOne);
    }

    internal void PlayNewHand()
    {
        PlayChipsSound();

        if (GameStatic.UI.currentPlayerMoney > 5000)
        {
            _DealerVOSource.Play(lsHighBet[2]);
        }
        else if (GameStatic.UI.currentPlayerMoney > 1000)
        {
            _DealerVOSource.Play(lsHighBet[1]);
        }
        else if (GameStatic.UI.currentPlayerMoney > 500)
        {
            _DealerVOSource.Play(lsHighBet[0]);
        }
        else
        {
            PlayRandomAudioFromArray(lsNewHand);
        }
    }

    internal void PlayPush()
    {
        PlayRandomAudioFromArray(lsPush);
    }

    #endregion

    #region GAMING

    public void PlayWinGaming()
    {
        GamingSource.PlayOneShot(fxWin);
    }

    #endregion

    #region CHIPS

    public void PlayChipsSound()
    {
        ChipSource.PlayOneShot(fxChips);
    }

    #endregion
}
