﻿using UnityEngine;
using System.Collections;
using VRStandardAssets.Utils;
using System;

public class GameManager : MonoBehaviour {

    public bool IsDebug;
    public bool FollowPlayer;
    public bool KeyboardControl;
    public int InsuranceWaitTime;
    public Deck theDeck;
    public Dealer theDealer;
    public Table theTable;
    public Camera mainCamera;
    public GameObject tempCardParent;    

	// Use this for initialization
	void Awake () {
        GameStatic.IsDebug = IsDebug;
        GameStatic.FollowPlayer = FollowPlayer;
        GameStatic.KeyboardControl = KeyboardControl;
        GameStatic.InsuranceWaitTime = InsuranceWaitTime;
        GameStatic.TempCardParent = tempCardParent;
        GameStatic.gameManager = this;
        GameStatic.MainCamera = mainCamera;
        GameStatic.StartGame();
    }

    void Start()
    {
        GameStatic.metaGame.ResetLevel();
    }

    internal void ChangeCards(Card currCard)
    {
        for (int i = 0; i < GameStatic.TempCardParent.transform.childCount; i++)
        {
            Destroy(GameStatic.TempCardParent.transform.GetChild(i));
        }

        var asd = GameStatic.CardPlacementManager.CardObjects.GetCard(currCard);
        asd.transform.position = new Vector3();
        asd.transform.rotation = new Quaternion();
        asd.transform.SetParent(GameStatic.TempCardParent.transform);
    }
}