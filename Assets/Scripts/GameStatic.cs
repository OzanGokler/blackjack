﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System;
using VRStandardAssets.Utils;

public class GameStatic
{
    public static bool IsDebug = false;
    public static UI UI;
    public static Table table;
    public static TestUIText testUItext;
    public static MetaGame metaGame;
    public static GameManager gameManager;
    public static AnimationManager animationManager;
    public static CardPlacement CardPlacementManager;
    public static SwipeManager swipeManager;
    public static AudioManager AudioManager;
    public static bool IsSplit = false;

    public static animState state= animState.Start;
    public static Camera MainCamera;
    internal static Card TempCard;
    internal static GameObject TempCardParent;

    public static bool IsWaitingForInsuranceResponse = false;
    internal static bool FollowPlayer;
    internal static bool KeyboardControl;
    internal static int InsuranceWaitTime;

    public enum animState
    {
        Start,
        InitialDeal,
        PlayerPicking,
        DealerPicking,
        HandOver,
        None
    }    
    
    public static void StartGame()
    {
        metaGame = new MetaGame();
        gameManager.theDeck.LoadNewSet();
    }

    public static void Begin()
    {
        animationManager.HandStart();
        state = animState.InitialDeal;
    }

    internal static void DealtCardTo(CardPosition pos)
    {
        metaGame.DealtCard(pos);
    }

    public static void Finish()
    {
        animationManager.HandFinish();      
    }
    
    public static void GetDealerCard()
    {
        animationManager.GetDealerCard();
    }

    internal static void HandleInsuranceLoop()
    {
        metaGame.HandleInsuranceLoop();
    }

    public static void PlayerHit()
    {
        animationManager.GetPlayerCard();
    }

    internal static void ChangeCardTexture()
    {
        TempCardParent.GetComponent<SkinnedMeshRenderer>().material.SetTextureOffset("_MainTex", metaGame.lastgivenCard.GettextureOffset());
    }

    internal static void GetSplitCard()
    {
        animationManager.GetSplitCard();
    }
    
    public static void GatherCards()
    {
        metaGame.GatherCards();
    }    

    internal static void PlayerWaiting()
    {
        state = animState.PlayerPicking;
        metaGame.PlayerHandControl();
    }

    public static void AddCardAndCalculateDealerPoints()
    {
        CardPlacementManager.AddCard(metaGame.GetLastDealerCard(), CardPosition.Dealer);
        metaGame.HandlePostCardDealt(CardPosition.Dealer);
    }

    public static void CalculateDealerPoints()
    {
        metaGame.HandlePostCardDealt(CardPosition.Dealer);
    }

    public static void AddCardAndCalculatePlayerPoints()
    {
        CardPlacementManager.AddCard(metaGame.GetLastPlayerCard(), CardPosition.Player);
        metaGame.HandlePostCardDealt(CardPosition.Player);
    }

    public static void AddCardAndCalculateSplitPoints()
    {
        CardPlacementManager.AddCard(metaGame.GetLastSplitCard(), CardPosition.PlayerSplit);
        metaGame.HandlePostCardDealt(CardPosition.PlayerSplit);
    }

    public static void StartDealerTurn()
    {
        state = animState.DealerPicking;
        animationManager.StartDealerTurn();
    }

    internal static void SplitStart()
    {
        animationManager.Dealer.SetBool("Split", true);
    }

    internal static void SplitCard()
    {
        metaGame.SplitCard();
        CardPlacementManager.SplitCard();
    }

    internal static void Stick()
    {
        metaGame.Stick();
    }

    internal static void HandleSplitFinish()
    {
        metaGame.HandleSplitStand();
    }

    internal static void HandleInsuranceResponse(bool v)
    {
        metaGame.HandleInsuranceResponse(v);
    }
}
