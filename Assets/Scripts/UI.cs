﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;

public class UI : MonoBehaviour
{
    public double currentPlayerMoney;
    public TextMesh txtBet;
    public GameObject UI12;
    public GameObject UI14;

    public GameObject imgBetMounts;
    public GameObject imgDeal;
    public GameObject imgClear;

    public GameObject imgHit;
    public GameObject imgStand;
    public GameObject imgSplit;

    public GameObject imgYes;
    public GameObject imgNo;
    public GameObject imgInsuranceTitle;
    
    private List<GameObject> Begin_UI_List;
    private List<GameObject> Selector_IU_List;
    private List<GameObject> Hit_and_StandList_UI_List;
    private List<GameObject> Insurance_List;

    int BeginUIindex = 0;
    int HitandStandindex = 0;

    public void Awake()
    {
        GameStatic.UI = this;
    }

    public void Start()
    {
        GameStatic.swipeManager.OnClick += SwipeManager_OnClick;
        GameStatic.swipeManager.OnSwipe += SwipeManager_OnSwipe;
        GameStatic.metaGame.Reset += MetaGame_Reset;

        currentPlayerMoney = GameStatic.metaGame.PlayerMoney;

        Begin_UI_List = new List<GameObject>();
        Hit_and_StandList_UI_List = new List<GameObject>();
        Insurance_List = new List<GameObject>();
        Selector_IU_List = new List<GameObject>();

        Begin_UI_List.Add(imgDeal);
        Begin_UI_List.Add(imgClear);
        Begin_UI_List.Add(imgBetMounts);

        Hit_and_StandList_UI_List.Add(imgHit);
        Hit_and_StandList_UI_List.Add(imgStand);

        Insurance_List.Add(imgYes);
        Insurance_List.Add(imgNo);
        Insurance_List.Add(imgInsuranceTitle);

        BeginUIOpen();
        BeginUIindexCalculate();

        txtBet.text = "0";

        if (GameStatic.FollowPlayer)
        {
            transform.LookAt(GameStatic.MainCamera.transform);
        }

        if (GameStatic.KeyboardControl)
        {
            StartCoroutine(InputUpdate());
        }
    }

    private void MetaGame_Reset()
    {
        txtBet.text = "0";
        BeginUIOpen();
        BeginUIindexCalculate();
        hitAndStandUIClose();
        BeginUIindex = 0;
        HitandStandindex = 0;
        BeginUIOpen();
        BeginUIindexCalculate();
    }

    private void SwipeManager_OnSwipe(SwipeDirection dir)
    {
        switch (dir)
        {
            case SwipeDirection.Up:
                ScrollUp();
                GameStatic.AudioManager.PlayUINavigation();
                break;
            case SwipeDirection.Down:
                ScrollDown();
                GameStatic.AudioManager.PlayUINavigation();
                break;
            default:
                break;
        }
    }

    private void SwipeManager_OnClick()
    {
        Select();
        GameStatic.AudioManager.PlayUISelect();
    }

    public void BeginUIindexCalculate()
    {
        if (BeginUIindex < 6)
        {
            UI12.SetActive(true);
            UI14.SetActive(false);
        }
        else
        {
            UI12.SetActive(false);
            UI14.SetActive(true);
        }

        switch (BeginUIindex)
        {
            case 0: GameStatic.animationManager.UI_bet5(); break;
            case 1: GameStatic.animationManager.UI_bet25(); break;
            case 2: GameStatic.animationManager.UI_bet100(); break;
            case 3: GameStatic.animationManager.UI_bet500(); break;
            case 4: GameStatic.animationManager.UI_bet1000(); break;
            case 5: GameStatic.animationManager.UI_bet5000(); break;
            case 6: GameStatic.animationManager.UI_Deal(); break;
            case 7: GameStatic.animationManager.UI_Clear(); break;
            default:
                throw new System.ArgumentOutOfRangeException("BeginUIIndex is out of range. Value: " + BeginUIindex);
        }
    }

    public void HitandStandUIindexCalculate()
    {
        UI12.SetActive(false);
        UI14.SetActive(true);

        if (HitandStandindex == 0)
        {
            GameStatic.animationManager.UI_Stand();
        }
        else if (HitandStandindex == 1)
        {
            GameStatic.animationManager.UI_Hit();
        }
        else if (HitandStandindex == 2)
        {
            GameStatic.animationManager.UI_Split();
        }
    }

    public void InsuranceCalculate()
    {
        UI12.SetActive(false);
        UI14.SetActive(true);

        if (HitandStandindex == 0)
        {
            GameStatic.animationManager.UI_Yes();
        }
        else if (HitandStandindex == 1)
        {
            GameStatic.animationManager.UI_No();
        }
    }

    public void ScrollUp()
    {
        if (GameStatic.state == GameStatic.animState.Start)
        {
            BeginUIindex++;
            if (BeginUIindex == 8)
            {
                BeginUIindex = 0;
            }
            BeginUIindexCalculate();
        }
        else if (GameStatic.state == GameStatic.animState.InitialDeal && GameStatic.IsWaitingForInsuranceResponse)
        {
            HitandStandindex++;
            if (HitandStandindex >= Insurance_List.Count - 1)
            {
                HitandStandindex = 0;
            }
            else
            {
                InsuranceCalculate();
            }
        }
        else if (GameStatic.state == GameStatic.animState.PlayerPicking)
        {
            HitandStandindex++;
            if (HitandStandindex >= Hit_and_StandList_UI_List.Count)
            {
                HitandStandindex = 0;
            }
            else
            {
                HitandStandUIindexCalculate();
            }
        }
    }

    public void ScrollDown()
    {
        if (GameStatic.state == GameStatic.animState.Start)
        {
            BeginUIindex--;
            if (BeginUIindex == -1)
            {
                BeginUIindex = 7;
            }
            BeginUIindexCalculate();
        }
        else if (GameStatic.state == GameStatic.animState.InitialDeal && GameStatic.IsWaitingForInsuranceResponse)
        {
            HitandStandindex--;
            if (HitandStandindex == -1)
            {
                HitandStandindex = Insurance_List.Count - 2;
            }
            else
            {
                InsuranceCalculate();
            }
        }
        else if (GameStatic.state == GameStatic.animState.PlayerPicking)
        {
            HitandStandindex--;
            if (HitandStandindex == -1)
            {
                HitandStandindex = Hit_and_StandList_UI_List.Count - 1;
            }
            else
            {
                HitandStandUIindexCalculate();
            }
        }
    }

    public void Select()
    {
        if (GameStatic.state == GameStatic.animState.Start)
        {
            switch (BeginUIindex)
            {
                case 0: Select5Bet(); break;
                case 1: Select25Bet(); break;
                case 2: Select100Bet(); break;
                case 3: Select500Bet(); break;
                case 4: Select1000Bet(); break;
                case 5: Select5000Bet(); break;
                case 6: SelectDeal(); break;
                case 7: SelectClear(); break;
            }
        }
        else if (GameStatic.state == GameStatic.animState.InitialDeal && GameStatic.IsWaitingForInsuranceResponse) //For Insurance
        {
            switch (HitandStandindex)
            {
                case 0: SelectStand(); break;
                case 1: SelectHit(); break;
            }
        }
        else if (GameStatic.state == GameStatic.animState.PlayerPicking)
        {
            switch (HitandStandindex)
            {
                case 0: SelectStand(); break;
                case 1: SelectHit(); break;
                case 2: SelectSplit(); break;
            }
        }

    }

    public static void SelectSplit()
    {
        GameStatic.animationManager.UI_Split_onPress();
        GameStatic.SplitStart();
    }

    public static void SelectHit()
    {
        GameStatic.animationManager.UI_Hit_onPress();

        if (GameStatic.IsWaitingForInsuranceResponse)
        {
            GameStatic.HandleInsuranceResponse(false);
        }
        else if (GameStatic.IsSplit)
        {
            GameStatic.GetSplitCard();
        }
        else
        {
            GameStatic.PlayerHit();
        }
    }

    public static void SelectStand()
    {
        GameStatic.animationManager.UI_Stand_onPress();

        if (GameStatic.IsWaitingForInsuranceResponse)
        {
            GameStatic.HandleInsuranceResponse(true);
        }
        else if (GameStatic.IsSplit)
        {
            GameStatic.HandleSplitFinish();
        }
        else
        {
            GameStatic.Stick();
        }
    }

    public void SelectClear()
    {
        GameStatic.animationManager.UI_Clear_onPress();
        GameStatic.gameManager.theDealer.ClearBets();
        currentPlayerMoney = GameStatic.metaGame.PlayerMoney;
            txtBet.text = GameStatic.gameManager.theDealer.currentTotalChip.ToString();
    }

    public void SelectDeal()
    {
        GameStatic.animationManager.UI_Deal_onPress();
        GameStatic.AudioManager.PlayNewHand();
        GameStatic.Begin();
        GameStatic.metaGame.PlayerMoney = currentPlayerMoney;
        GameStatic.table.UpdateChipText(currentPlayerMoney);
        hitAndStandUIOpen();
        HitandStandUIindexCalculate();
        BeginUIClose();
    }

    public void Select5000Bet()
    {
        if (5000 <= currentPlayerMoney)
        {
            currentPlayerMoney -= 5000;
            GameStatic.gameManager.theDealer.PlayerChips[0].FiveThousand += 1;
            GameStatic.animationManager.UI_bet5000_onPress();
            GameStatic.gameManager.theDealer.currentTotalChip += 5000;
            txtBet.text = GameStatic.gameManager.theDealer.currentTotalChip.ToString();
        }
    }

    public void Select1000Bet()
    {
        if (1000 <= currentPlayerMoney)
        {
            currentPlayerMoney -= 1000;
            GameStatic.gameManager.theDealer.PlayerChips[0].Thousand += 1;
            GameStatic.animationManager.UI_bet1000_onPress();
            GameStatic.gameManager.theDealer.currentTotalChip += 1000;
            txtBet.text = GameStatic.gameManager.theDealer.currentTotalChip.ToString();
        }
    }

    public void Select500Bet()
    {
        if (500 <= currentPlayerMoney)
        {
            currentPlayerMoney -= 500;
            GameStatic.gameManager.theDealer.PlayerChips[0].FiveHundred += 1;
            GameStatic.animationManager.UI_bet500_onPress();
            GameStatic.gameManager.theDealer.currentTotalChip += 500;
            txtBet.text = GameStatic.gameManager.theDealer.currentTotalChip.ToString();
        }
    }

    public void Select100Bet()
    {
        if (100 <= currentPlayerMoney)
        {
            currentPlayerMoney -= 100;
            GameStatic.gameManager.theDealer.PlayerChips[0].Hundred += 1;
            GameStatic.animationManager.UI_bet100_onPress();
            GameStatic.gameManager.theDealer.currentTotalChip += 100;
            txtBet.text = GameStatic.gameManager.theDealer.currentTotalChip.ToString();
        }
    }

    public void Select25Bet()
    {
        if (25 <= currentPlayerMoney)
        {
            currentPlayerMoney -= 25;
            GameStatic.gameManager.theDealer.PlayerChips[0].TwentyFive += 1;
            GameStatic.animationManager.UI_bet25_onPress();
            GameStatic.gameManager.theDealer.currentTotalChip += 25;
            txtBet.text = GameStatic.gameManager.theDealer.currentTotalChip.ToString();
        }
    }

    public void Select5Bet()
    {
        if (5 <= currentPlayerMoney)
        {
            currentPlayerMoney -= 5;
            GameStatic.gameManager.theDealer.PlayerChips[0].Five += 1;
            GameStatic.animationManager.UI_bet5_onPress();
            GameStatic.gameManager.theDealer.currentTotalChip += 5;
            txtBet.text = GameStatic.gameManager.theDealer.currentTotalChip.ToString();
        }
    }

    public void InsuranceUIOpen()
    {
        hitAndStandUIClose();
        for (int i = 0; i < Insurance_List.Count; i++)
        {
            Insurance_List[i].SetActive(true);
        }
        HitandStandindex = 0;
        InsuranceCalculate();
    }

    public void InsuranceUIClose()
    {      
        for (int i = 0; i < Insurance_List.Count; i++)
        {
            Insurance_List[i].SetActive(false);
        }
        hitAndStandUIOpen();
    }

    public void BeginUIOpen()
    {
        for (int i = 0; i < Begin_UI_List.Count; i++)
        {
            Begin_UI_List[i].SetActive(true);
        }
    }

    public void BeginUIClose()
    {
        for (int i = 0; i < Begin_UI_List.Count; i++)
        {
            Begin_UI_List[i].SetActive(false);
        }
    }

    public void hitAndStandUIOpen()
    {
        for (int i = 0; i < Hit_and_StandList_UI_List.Count; i++)
        {
            Hit_and_StandList_UI_List[i].SetActive(true);
        }
        HitandStandUIindexCalculate();
    }

    public void hitAndStandUIClose()
    {
        for (int i = 0; i < Hit_and_StandList_UI_List.Count; i++)
        {
            Hit_and_StandList_UI_List[i].SetActive(false);
        }
    }

    public void SplitUIOpen()
    {
        if (!Hit_and_StandList_UI_List.Contains(imgSplit))
        {
            hitAndStandUIClose();
            Hit_and_StandList_UI_List.Add(imgSplit);
            hitAndStandUIOpen();
        }
    }

    public void SplitUIClose()
    {
        hitAndStandUIClose();
        Hit_and_StandList_UI_List.Remove(imgSplit);
        HitandStandindex = 0;
        HitandStandUIindexCalculate();
    }
    
    public IEnumerator InputUpdate()
    {
        while (true)
        {
            if (Input.GetKeyDown(KeyCode.H) && GameStatic.state == GameStatic.animState.PlayerPicking)
            {
                SelectHit();
            }
            //GetPlayerCard
            else if (Input.GetKeyDown(KeyCode.S) && GameStatic.state == GameStatic.animState.PlayerPicking)
            {
                SelectStand();
            }
            //ok-Stick
            else if (Input.GetKeyDown(KeyCode.P) && GameStatic.state == GameStatic.animState.PlayerPicking)
            {
                SelectSplit();
            }
            else if (Input.GetKeyDown(KeyCode.C) && GameStatic.state == GameStatic.animState.Start)
            {
                SelectClear();
            }
            else if (Input.GetKeyDown(KeyCode.D) && GameStatic.state == GameStatic.animState.Start)
            {
                SelectDeal();
            }
            //chip 5
            else if (Input.GetKeyDown(KeyCode.F1) && GameStatic.state == GameStatic.animState.Start)
            {
                Select5Bet();
            }
            //chip 25
            else if (Input.GetKeyDown(KeyCode.F2) && GameStatic.state == GameStatic.animState.Start)
            {
                Select25Bet();
            }
            //chip 100
            else if (Input.GetKeyDown(KeyCode.F3) && GameStatic.state == GameStatic.animState.Start)
            {
                Select100Bet();
            }
            //chip 500
            else if (Input.GetKeyDown(KeyCode.F4) && GameStatic.state == GameStatic.animState.Start)
            {
                Select500Bet();
            }
            //chip 1000
            else if (Input.GetKeyDown(KeyCode.F5) && GameStatic.state == GameStatic.animState.Start)
            {
                Select1000Bet();
            }
            //chip 5000
            else if (Input.GetKeyDown(KeyCode.F6) && GameStatic.state == GameStatic.animState.Start)
            {
                Select5000Bet();
            }
            yield return null;
        }
    }
}

public enum UIType
{
    Initial,
    HitAndStand,
    Split,
    Insurance
}

public class UIGroup
{
    public List<GameObject> UiElements;
    public UIType Type;
    public bool IsActive;

    public UIGroup(UIType type)
    {
        Type = type;
    }

    public void Add(GameObject obj)
    {
        UiElements.Add(obj);
    }

    public void SetActive(bool v)
    {
        for (int i = 0; i < UiElements.Count; i++)
        {
            UiElements[i].SetActive(v);
        }
    }
}
