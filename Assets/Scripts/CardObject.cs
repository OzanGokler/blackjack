﻿using UnityEngine;
using System.Collections;

public class CardObject : MonoBehaviour {

    public GameObject Front;
    public GameObject Back;
    [HideInInspector]
    public CardStatus Status;

    private Texture2D _frontTexture;
    [HideInInspector]
    public Texture2D FrontTexture
    {
        get
        {
            if(_frontTexture == null)
            {
                _frontTexture = GetComponentInChildren<SpriteRenderer>().sprite.texture;
            }

            return _frontTexture;
        }
    }

    private Texture2D _backtexture;
    [HideInInspector]
    public Texture2D BackTexture
    {
        get
        {
            if (_backtexture == null)
            {
                _backtexture = GetComponentInChildren<SpriteRenderer>().sprite.texture;
            }

            return _backtexture;
        }
    }

    void Start()
    {
    }
    
    public void SetLocation(int Location)
    {        
        Front.GetComponent<SpriteRenderer>().sortingOrder = -10 + Location;
        Back.GetComponent<SpriteRenderer>().sortingOrder = -10 + Location;
    }

}

public enum CardStatus
{
    Open,
    Closed
}
