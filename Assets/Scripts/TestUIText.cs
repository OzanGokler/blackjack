﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class TestUIText : MonoBehaviour {
    public Text DealerPoint;
    public Text PlayerPoint;
    public Text ResultText;
    public Text BetText;
    public Text Money;
    
    void Start ()
    {
        if (GameStatic.IsDebug)
        {
            GameStatic.testUItext = this;
            GameStatic.swipeManager.OnClick += SwipeManager_OnClick;
            GameStatic.metaGame.Reset += MetaGame_Reset;
        }
        else
        {
            gameObject.SetActive(false);
        }
    }

    private void MetaGame_Reset()
    {
        //DealerPoint.text = "Dealer: ";
        //PlayerPoint.text = "Player: ";
        //ResultText.text = "Result: ";
        //BetText.text = "Bet: 0";
    }

    private void SwipeManager_OnClick()
    {
        BetText.text = ("Bet: " + GameStatic.gameManager.theDealer.currentTotalChip.ToString());
    }
       
}
