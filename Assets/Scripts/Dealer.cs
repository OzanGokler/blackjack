﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
public class Dealer : MonoBehaviour {

    public List<Chip> PlayerChips;
    public int currentTotalChip = 0;

    void Awake()
    {
        PlayerChips = new List<Chip>();
        PlayerChips.Add(new Chip());
    }

    void Start()
    {
        GameStatic.metaGame.Reset += MetaGame_Reset;        
    }

    private void MetaGame_Reset()
    {
        ClearBets();
    }
        
    public void ClearBets()
    {
        currentTotalChip = 0;
        PlayerChips[0].Five = 0;
        PlayerChips[0].TwentyFive = 0;
        PlayerChips[0].Hundred= 0;
        PlayerChips[0].FiveHundred = 0;
        PlayerChips[0].Thousand = 0;
        PlayerChips[0].FiveThousand = 0;
    }
    
    #region ANIMATION EVENT FUNCTIONS

    public void DealDealerCard()
    {
        GameStatic.AddCardAndCalculateDealerPoints();
    }

    public void DealDealerEnter()
    {
        if (GameStatic.metaGame.IsSecondDealerCard)
        {
            GameStatic.animationManager.PlayDealDealerCard2();
        }

        GameStatic.DealtCardTo(CardPosition.Dealer);
    }

    public void DealPlayerCard()
    {
        GameStatic.AddCardAndCalculatePlayerPoints();
    }

    public void DealPlayerEnter()
    {
        GameStatic.DealtCardTo(CardPosition.Player);
    }

    public void ChangeCardTexture()
    {
        GameStatic.ChangeCardTexture();
    }

    public void FlipDealerCard()
    {
        //Debug.Log("FlipDealerCard");
        GameStatic.CalculateDealerPoints();
    }

    public void InsuranceLoop()
    {
        GameStatic.HandleInsuranceLoop();
    }

    public void FlipDealerEnter()
    {
        GameStatic.animationManager.PlayFlipCard();
    }

    public void GatherCards()
    {
        GameStatic.GatherCards();
    }

    public void GatherPlayerCards()
    {
        GameStatic.CardPlacementManager.ClearPlayerCards();
        GameStatic.CardPlacementManager.ClearSplitCards();
    }

    public void GatherDealerCards()
    {
        GameStatic.CardPlacementManager.ClearDealerCards();
    }

    public void IdlePlayerDecide()
    {
        GameStatic.PlayerWaiting();
    }

    public void SplitPlayerCard()
    {
        GameStatic.SplitCard();
    }

    public void DealSplitCard()
    {
        GameStatic.AddCardAndCalculateSplitPoints();
    }

    public void DealSplitEnter()
    {
        GameStatic.DealtCardTo(CardPosition.PlayerSplit);
    }

    public void PlayFlipSound()
    {
        GameStatic.AudioManager.PlayCardFlip();
    }

    public void PlayCardToCardSound()
    {
        GameStatic.AudioManager.PlayPlaceCard();
    }

    public void PlayShoeSound()
    {
        GameStatic.AudioManager.PlayCardShoe();
    }

    public void PlayCardToTableSound()
    {
        GameStatic.AudioManager.PlayPlaceCardTable();
    }

    public void PlayGatherCardsSound()
    {
        GameStatic.AudioManager.PlayGatherCards();
    }

    #endregion


}
