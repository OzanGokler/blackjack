﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;

public class MetaGame
{
    public event Action Reset;

    public double PlayerMoney = 50;

    private System.Random random;

    private Hand DealerHand;
    private Hand PlayerHand;
    private Hand SplitHand;

    private Hand currentHand = null;

    public Card lastgivenCard;

    public List<Card> DebugCards = new List<Card>
    {
        //Dealer and Player Starts with Blackjack
        //new Card {number = 1, suit = Card.Suit.Spades },
        //new Card {number = 13, suit = Card.Suit.Hearts },
        //new Card {number = 13, suit = Card.Suit.Spades },
        //new Card {number = 1, suit = Card.Suit.Hearts },

        //Dealer starts with Blackjack
        //new Card {number = 11, suit = Card.Suit.Clubs },
        //new Card {number = 1, suit = Card.Suit.Spades },
        //new Card {number = 4, suit = Card.Suit.Hearts },
        //new Card {number = 13, suit = Card.Suit.Spades },

        //Player Starts with Blackjack
        //new Card {number = 1, suit = Card.Suit.Spades },
        //new Card {number = 2, suit = Card.Suit.Diamonds },
        //new Card {number = 13, suit = Card.Suit.Spades },
        //new Card {number = 3, suit = Card.Suit.Hearts },

        //SplitOpen
        new Card {number = 6, suit = Card.Suit.Clubs},
        new Card {number = 2, suit = Card.Suit.Diamonds},
        new Card {number = 6, suit = Card.Suit.Diamonds},
        new Card {number = 3, suit = Card.Suit.Hearts},
        new Card {number = 5, suit = Card.Suit.Hearts},
        new Card {number = 10, suit = Card.Suit.Hearts},
    };

    public bool IsSecondDealerCard
    {
        get { return DealerHand.Cards.Count == 1; }
    }


    public MetaGame()
    {
        PlayerHand = new Hand(CardPosition.Player);
        DealerHand = new Hand(CardPosition.Dealer);
        random = new System.Random();
    }

    internal void DealtCard(CardPosition pos)
    {
        Card currCard = GetCard();
        lastgivenCard = currCard;

        switch (pos)
        {
            case CardPosition.Dealer:
                DealerHand.Cards.Add(currCard);
                break;
            case CardPosition.Player:
                PlayerHand.Cards.Add(currCard);
                break;
            case CardPosition.PlayerSplit:
                SplitHand.Cards.Add(currCard);
                break;
            default:
                break;
        }

        GameStatic.animationManager.AddCardCount(pos);
    }

    private Card GetCard()
    {
        Card currCard = null;
        if (GameStatic.IsDebug && DebugCards.Count > 0)
        {
            currCard = DebugCards[0];
            DebugCards.RemoveAt(0);
        }
        else
            currCard = SafeGetCard();
        return currCard;
    }

    public Card SafeGetCard()
    {
        return GameStatic.gameManager.theDeck.GetSafeCard();
    }

    public void ResetLevel()
    {
        PlayerHand.ResetHand();
        if (SplitHand != null) SplitHand.ResetHand();
        DealerHand.ResetHand();
        currentHand = null;

        InsuranceWaitCount = 0;

        GameStatic.IsSplit = false;
        GameStatic.IsWaitingForInsuranceResponse = false;

        if (Reset != null)
        {
            Reset();
        }
    }

    int InsuranceWaitCount = 0;
    public void HandlePostCardDealt(CardPosition pos)
    {
        GameStatic.TempCardParent.GetComponent<SkinnedMeshRenderer>().material.SetTextureOffset("_MainTex", Vector2.zero);

        if (GameStatic.state == GameStatic.animState.InitialDeal)
        {
            if (DealerHand.Cards.Count == 2)
            {
                HandleInitialDeal();
            }
            else if (GameStatic.IsWaitingForInsuranceResponse)
            {
                PlayerHandControl();
            }
        }
        else if (GameStatic.state == GameStatic.animState.PlayerPicking)
        {
            PlayerHandControl();
        }
        else if (GameStatic.state == GameStatic.animState.DealerPicking)
        {
            HandleFinish();
        }
        else
        {
            throw new InvalidOperationException();
        }
    }

    internal void SplitCard()
    {
        PlayerMoney -= GameStatic.gameManager.theDealer.currentTotalChip;
        GameStatic.gameManager.theTable.UpdateChipText(PlayerMoney);

        SplitHand = new Hand(CardPosition.PlayerSplit);
        GameStatic.IsSplit = true;
        var card = PlayerHand.Cards[1];
        SplitHand.Cards.Add(card);
        PlayerHand.Cards.RemoveAt(1);

        currentHand = SplitHand;

        GameStatic.UI.SplitUIClose();
        GameStatic.UI.hitAndStandUIOpen();
    }

    private void HandleInitialDeal()
    {
        currentHand = PlayerHand;

        //Play audio for initial hand value
        GameStatic.AudioManager.PlayInitialHandValue(PlayerHand);

        if (PlayerHand.SoftValue == 21)
        {
            HandleFinish();
            return;
        }

        if (DealerHand.Cards[0].Value == 11)
        {
            if (DealerHand.HasInsurance && GameStatic.gameManager.theDealer.currentTotalChip / 2 < PlayerMoney)
            {
                GameStatic.animationManager.SetDealerInsurance(true);
                GameStatic.IsWaitingForInsuranceResponse = true;
                GameStatic.UI.InsuranceUIOpen();
                GameStatic.AudioManager.PlayOfferInsurance();
                return;
            }
            else
            {
                Stick();
            }
        }

        ControlSplit();
    }

    private void ControlSplit()
    {
        if (PlayerHand.CanSplit && GameStatic.gameManager.theDealer.currentTotalChip < PlayerMoney)
        {
            GameStatic.UI.SplitUIOpen();
        }
    }

    private void HandleFinish()
    {
        if (GameStatic.state == GameStatic.animState.InitialDeal)
        {
            FinishGame();
        }
        //Debug.Log("HandleFinish: " + cardPos);
        else if (GameStatic.state == GameStatic.animState.DealerPicking)
        {
            if (!IsDealerReachSoft17() && PlayerHand.SoftValue <= 21)
            {
                GameStatic.GetDealerCard();
            }
            else
            {
                FinishGame();
            }
        }
    }

    private void FinishGame()
    {
        double baseChipsWon = 0;

        //If player chose split, first calculate its result.
        if (SplitHand != null)
        {
            var splitResult = HandleHandCalculation(SplitHand);
            baseChipsWon += CalculateChipAmount(splitResult);

            //PlayAudio for splitHand
            GameStatic.AudioManager.PlayFinishHand(splitResult, SplitHand, DealerHand);
        }

        //Calculate player's result
        var result = HandleHandCalculation(PlayerHand);
        baseChipsWon += CalculateChipAmount(result);

        //PlayAudio for hand
        GameStatic.AudioManager.PlayFinishHand(result, PlayerHand, DealerHand);

        //If debug option selected log info
        if (GameStatic.IsDebug)
        {
            GameStatic.testUItext.Money.text = baseChipsWon.ToString();
        }

        //If dealer hand got insurance option, play that animation.
        if (DealerHand.HasInsurance)
        {
            GameStatic.animationManager.SetDealerBlackjackResult(result != HandState.Lose);
        }


        //Update chips
        PlayerMoney += baseChipsWon;
        GameStatic.UI.currentPlayerMoney = PlayerMoney;
        GameStatic.table.UpdateChipText(PlayerMoney);

        //Tell the animatiom manager to finish set
        GameStatic.Finish();
    }

    public double CalculateChipAmount(HandState state)
    {
        var baseAmount = GameStatic.gameManager.theDealer.PlayerChips[0].GetTotal();

        switch (state)
        {
            case HandState.None: throw new InvalidOperationException();
            case HandState.Win: return baseAmount;
            case HandState.Lose: return 0;
            case HandState.Blackjack: return baseAmount * 1.5;
            case HandState.Push: return 0;
        }

        throw new InvalidOperationException();
    }

    public HandState HandleHandCalculation(Hand other)
    {
        if (GameStatic.IsDebug)
        {
            GameStatic.testUItext.PlayerPoint.text = " Hand Total value: " + other.TotalValue + " Soft value: " + other.SoftValue;
            GameStatic.testUItext.DealerPoint.text = " Hand Total value: " + DealerHand.TotalValue + " Soft value: " + DealerHand.SoftValue;
        }
        Debug.Log(other.CardType + " Hand Total value: " + other.TotalValue);
        Debug.Log(other.CardType + " Hand Soft value: " + other.SoftValue);
        Debug.Log(DealerHand.CardType + " Hand Total value: " + DealerHand.TotalValue);
        Debug.Log(DealerHand.CardType + " Hand Total value: " + DealerHand.SoftValue);

        if (other.SoftValue == 21)
        {
            if (GameStatic.state == GameStatic.animState.InitialDeal || DealerHand.SoftValue == 21)
            {
                return HandState.Blackjack;
            }
            else
            {
                return HandState.Win;
            }
        }

        if (other.TotalValue == 21)
        {
            if (DealerHand.TotalValue == 21)
            {
                return HandState.Push;
            }
            else
            {
                return HandState.Win;
            }
        }
        else if (other.TotalValue > 21)
        {
            return HandState.Lose;
        }
        else if (other.TotalValue < 21)
        {
            if (DealerHand.SoftValue == 21)
            {
                return HandState.Lose;
            }
            else if (DealerHand.TotalValue > 21)
            {
                return HandState.Win;
            }
            else if (DealerHand.TotalValue == other.TotalValue)
            {
                return HandState.Push;
            }
            else if (DealerHand.TotalValue > other.TotalValue)
            {
                return HandState.Lose;
            }
            else if (DealerHand.TotalValue < other.TotalValue)
            {
                return HandState.Win;
            }
        }


        return HandState.None;
    }

    public Card GetLastDealerCard()
    {
        return DealerHand.LastCard;
    }

    public Card GetLastPlayerCard()
    {
        return PlayerHand.LastCard;
    }

    internal Card GetLastSplitCard()
    {
        return SplitHand.LastCard;
    }


    public void PlayerHandControl()
    {
        if (currentHand.TotalValue >= 21)
        {
            if (currentHand.CardType == CardPosition.PlayerSplit)
            {
                HandleSplitStand();
            }
            else if (currentHand.CardType == CardPosition.Player)
            {
                Stick();
            }
        } 
    }

    internal void HandleInsuranceLoop()
    {
        if (GameStatic.IsWaitingForInsuranceResponse)
        {
            InsuranceWaitCount++;

            if (InsuranceWaitCount == GameStatic.InsuranceWaitTime)
            {
                HandleInsuranceResponse(false);
            }
        }
    }

    public void HandleInsuranceResponse(bool isSelected)
    {
        GameStatic.IsWaitingForInsuranceResponse = false;
        GameStatic.animationManager.SetDealerInsurance(false);
        if (isSelected)
        {
            PlayerMoney -= GameStatic.gameManager.theDealer.currentTotalChip / 2;
            GameStatic.gameManager.theTable.UpdateChipText(PlayerMoney);

            if (DealerHand.SoftValue == 21)
            {
                PlayerMoney += GameStatic.gameManager.theDealer.currentTotalChip * 1.5;
                Stick();
            }
        }
        else
        {
            if (DealerHand.SoftValue == 21)
            {
                //Insurance successed
                Stick();
            }
            GameStatic.IsWaitingForInsuranceResponse = false;
        }
        InsuranceWaitCount = 0;
        GameStatic.UI.InsuranceUIClose();
        GameStatic.UI.hitAndStandUIOpen();
    }

    public void HandleSplitStand()
    {
        GameStatic.animationManager.AfterSplit();
        GameStatic.IsSplit = false;
        currentHand = PlayerHand;
        GameStatic.PlayerHit();
    }

    private double CalculateCurrentChips(bool isWin, bool isBlackjack)
    {
        int offset = isWin ? 1 : -1;
        double currentChipPoint = 0;
        for (int i = 0; i < GameStatic.gameManager.theDealer.PlayerChips.Count; i++)
        {
            currentChipPoint += offset * GameStatic.gameManager.theDealer.PlayerChips[i].GetTotal();
        }

        return isBlackjack ? currentChipPoint * 1.5f : currentChipPoint;
    }

    public void PlayerTie(Hand other)
    {
        if (GameStatic.IsDebug)
        {
            GameStatic.testUItext.ResultText.text = ("Result: Tie");
        }
        GameStatic.UI.currentPlayerMoney = PlayerMoney;
        GameStatic.Finish();
    }

    public void Stick()
    {
        GameStatic.StartDealerTurn();
    }

    public bool IsDealerReachSoft17()
    {
        if (DealerHand.TotalValue < 17)
        {
            return false;
        }
        else
            return true;
    }


    public void GatherCards()
    {
        ResetLevel();
        GameStatic.state = GameStatic.animState.Start;
    }
}

